Tayga Stateful
==============

This is a simple lab to use Tayga in stateful mode. Tayga is made stateful
using iptables as NAT44. DNS64 is implemented using BIND9.

Network
-------
The network is composed of two IPv6-only hosts, two IPv4-only hosts, a
stateful NAT64 and a DNS64 having an interface in each subnet.

```

                2001:db8:1::1/96 /-(NAT64)-\ 192.168.0.1/24
                                /           \
2001:db8:1::2/96 (Host IPv6) ---|           |--- (Host IPv4) 192.168.0.2/24
2001:db8:1::3/96 (Host IPv6) ---|           |--- (Host IPv4) 192.168.0.3/24
                                \           /
              2001:db8:1::100/96 \-(DNS64)-/ 192.168.0.10/24
```

Testing
-------
Start lab issuing `./start_lab`.
On an IPv6-only machine, try:

    ping6 2001:db8:1:ffff::192.198.0.2
    ping6 nip.com

On an IPv4-only machine, try:

    ping 198.168.255.233
    ping 198.168.255.234

*N.B. Each ipv6 machine is mapped on a pool of ipv4*
