# Deploy a NAT64 on Netkit (and DNS64) #

The goal of this project is deploying a NAT64 and DNS64 server in Netkit, hence 
providing a translation interface between an IPv4 and an IPv6 network. 

Two common implementations of NAT64 are **Jool** and **Tayga**. We deployed
both of them on Netkit, as described in the Wiki.


## Instructions ##
The repository is organized as follows:

* documents -> all project documentation
* sources -> source code (please create subfolders as needed)
* other -> every other thing

The documents folder is organized in subfolders

* report -> main report document
* presentation -> presentation
* notes -> please add any notes, intermediate results
* related -> you can keep here external documents