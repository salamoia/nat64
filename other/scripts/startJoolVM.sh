#!/bin/bash

#
# This script simply starts a Netkit VM to allow
# the installation of Jool (and other software).
#
# The VM is started and assigned an IP address on eth0 interface
# to allow Internet access (to download packages).
#
# Use '-W' option to make permanent changes to Netkit filesystem.
# 
# Consider using 'vclean --clean-all' after using this script
# to undo all host-side changes needed by '--eth0=tap' configuration (see 'man vstart').
#


# -----------------------------------------------------------------------------
# CONFIGURATION:
# -----------------------------------------------------------------------------

# IP Address of Host machine on tap interface
HOST_ADDRESS="10.0.0.2"
# IP Address to be set on Netkit VM eth0 interface 
GUEST_ADDRESS="10.0.0.3"
# Amount of memory reserved to Netkit VM
MEMORY_MB=512
# Name to assign to VM
VM_NAME="Jool"
# UML Kernel to use
UML_KERNEL=""

# -----------------------------------------------------------------------------



OPTION_W=""

# Parses CLI arguments, setting global variables
# with read values.
function _parseArguments()
{
	while getopts ":WH:G:k:" opt; do
		case $opt in
			W)
				OPTION_W="-W"
				;;
			k)
				UML_KERNEL="$OPTARG"
				;;
			H)
				HOST_ADDRESS=$OPTARG
				;;
			G)
				GUEST_ADDRESS=$OPTARG
				;;
			\?)
				echo "Invalid option: -$OPTARG" >&2
				_printUsage
				;;
		esac
	done
}

# Prints usage information about the script.
function _printUsage
{
	echo "Usage: startJoolVM.sh [ -W ] [-k <uml_kernel>] [-H <host_tap_IPaddress>] [-G <guest_eth0__IPaddress>]";
}



function main
{
	_parseArguments "$@"


	local kernel_opt="";
	if [[ "x$UML_KERNEL" != "x" ]]; then
		kernel_opt="-k $UML_KERNEL"
	fi

	vstart $kernel_opt -M ${MEMORY_MB} $OPTION_W --eth0=tap,${HOST_ADDRESS},${GUEST_ADDRESS} Jool --con0=this
}

main "$@"
