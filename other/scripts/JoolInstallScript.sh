# VM initialization
echo "nameserver 8.8.8.8" > /etc/resolv.conf
apt-get update




# Installation of Jool -- kernel modules
apt-get -y install build-essential linux-headers-486
mv /lib/modules/3.2.0-4-486 /lib/modules/`uname -r`
apt-get -y install dkms unzip

cd
wget https://github.com/NICMx/jool-doc/raw/gh-pages/download/Jool-3.4.2.zip
unzip Jool-3.4.2.zip

dkms install Jool-3.4.2




# Installation of Jool -- user applications
apt-get -y install libnl-3-dev autoconf pkg-config

cd Jool-3.4.2/usr
./configure
make
make install

